<?php
namespace autoload;
class AppClassLoader 
{

	public static function loadControllerRequires() {
		$baseDir = $_SERVER['DOCUMENT_ROOT'];
		require_once($baseDir.'/coco/Controllers/AppController.php');
		require_once($baseDir.'/coco/template/AppView.php');
	}

	public static function loadControllerRequired($controller) {
		$baseDir = $_SERVER['DOCUMENT_ROOT'];
		require($baseDir.'Controller/'.$controller.'Table.php');
	}

	public static function loadBaseControllerRequires() {
		require_once('coco/template/AppView.php');
		require_once('coco/Helpers/SessionHelper.php');
		require_once('coco/Helpers/CookieHelper.php');
	}

	public static function loadRouteRequired() {
		$baseDir = $_SERVER['DOCUMENT_ROOT'];
		require_once($baseDir.'/coco/config/Router/Router.php');
		require_once($baseDir.'/coco/config/Router/Routing.php');
	}

	public static function loadModelRequired($model) {
		require('Models/'.$model.'Table.php');
	}

	public static function loadBaseModel() {
		$baseDir = $_SERVER['DOCUMENT_ROOT'];
		require_once('coco/models/AppTable.php');
	}

	public static function loadIndexRequires() {
		require_once('coco/config/Route.php');
	}

	public static function loadDatabase() {
		require_once('coco/config/Settings.php');
		require_once('coco/database/DataBase.php');
		require_once('coco/database/QueryBuilder.php');
	}

	public static function loadBaseComponent() {
		require_once('coco/Controllers/Components/AppComponent.php');
	}

	public static function loadRoute() {
		require_once('coco/config/Router/Router.php');
		require_once('coco/config/Router/Routing.php');
	}



}


?>

