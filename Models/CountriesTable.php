<?php
namespace models;
require_once('AutoLoader.php');
use autoload\AppClassLoader;
AppClassLoader::loadBaseModel();

use coco\database\table\AppTable;

class CountriesTable extends AppTable
{
	function __construct() {
		parent::__construct('countries');
		$this->primary_key = 'country_id';
	}
}
?>