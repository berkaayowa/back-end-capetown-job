<?php
namespace models;
require_once('AutoLoader.php');
use autoload\AppClassLoader;
AppClassLoader::loadBaseModel();

use coco\database\table\AppTable;

class Job_categoriesTable extends AppTable
{
	function __construct() {
		parent::__construct('job_categories');
		$this->primary_key = 'category_id';
	}
}
?>