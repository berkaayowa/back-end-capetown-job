<?php
namespace models;
require_once('AutoLoader.php');
use autoload\AppClassLoader;
AppClassLoader::loadBaseModel();

use coco\database\table\AppTable;

class JobsTable extends AppTable
{
	function __construct() {
		parent::__construct('jobs');
		$this->primary_key = 'job_id';

		$this->contains = [
			'job_types'=>'type_id',
			'job_categories'=>'category_id',
			'cities'=>'city_id'

		];

		$this->keys = [
			'job_types'=>'job_type_id',
			'job_categories'=>'job_category_id',
			'cities'=>'job_city_id'
		];
	}
}

?>