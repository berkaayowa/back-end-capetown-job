<?php
namespace models;
require_once('AutoLoader.php');
use autoload\AppClassLoader;
AppClassLoader::loadBaseModel();

use coco\database\table\AppTable;

class UsersTable extends AppTable
{
	function __construct() {
		parent::__construct('users');
		$this->primary_key = 'user_id';
	}
}
?>