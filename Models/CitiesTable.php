<?php
namespace models;
require_once('AutoLoader.php');
use autoload\AppClassLoader;
AppClassLoader::loadBaseModel();

use coco\database\table\AppTable;

class CitiesTable extends AppTable
{
	function __construct() {
		parent::__construct('cities');
		$this->primary_key = 'city_id';
	}
}
?>