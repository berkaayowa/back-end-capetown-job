<?php
namespace models;
require_once('AutoLoader.php');
use autoload\AppClassLoader;
AppClassLoader::loadBaseModel();

use coco\database\table\AppTable;

class Job_typesTable extends AppTable
{
	function __construct() {
		parent::__construct('job_types');
		$this->primary_key = 'type_id';
	}
}
?>