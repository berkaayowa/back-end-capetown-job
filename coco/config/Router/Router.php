<?php
namespace coco\config\router;

class AppRouter
{
	protected $method;
	protected $params;
	protected $controller;
	private $server_object;

	function __construct($server)
	{
		$this->server_object = $server;
	}

	public function route($path, $call_back) {

		$route_object = $this->filter_url();

		if ($this->get_method() == 'GET') {
			if (isset($_GET)) {
				$route_object["options"] = $_GET;
				if (isset($route_object["params"])) {
					$params_ = explode("?", $route_object["params"][0]);
					$route_object["params"] = array_shift($params_);
				}
			}

			if($route_object["controller"] == trim(str_replace("/","",$path))) {
				if (is_callable($call_back)) {
					call_user_func($call_back, $route_object);
				} 
			} else {
				if (is_callable($call_back)) {
					call_user_func($call_back, $route_object);
				}
			}
		} elseif ($this->get_method() == 'POST') {
			if($route_object["controller"] == trim(str_replace("/","",$path))) {
				if (is_callable($call_back)) {
					call_user_func($call_back, $route_object);
				} 
			} else {
				if (is_callable($call_back)) {
					call_user_func($call_back, $route_object);
				}
			}
		}

		else {
			die('Error::method is not defined yet');
		}
	}

	public function post($path, $call_back) {
		
		$route_object = $this->filter_url();

		if ($this->get_method() == 'POST') {
			if($route_object["controller"] == trim(str_replace("/","",$path))) {
				if (is_callable($call_back)) {
					call_user_func($call_back, $route_object);
				} 
			} else {
				if (is_callable($call_back)) {
					call_user_func($call_back, $route_object);
				}
			}
		}

		else {
			die('Error::method is not POST');
		}
	}


	public function get($path, $call_back) {
		
		$route_object = $this->filter_url();

		if ($this->get_method() == 'GET') {
			if($route_object["controller"] == trim(str_replace("/","",$path))) {
				if (is_callable($call_back)) {
					call_user_func($call_back, $route_object);
				} 
			} else {
				if (is_callable($call_back)) {
					call_user_func($call_back, $route_object);
				}
			}
		}

		else {
			die('Error::method is not POST');
		}
	}

	private function get_method() {
		return $this->server_object['REQUEST_METHOD'];
	}

	// public function filter_url() {
	// 	$url = explode('/', $this->server_object["PATH_INFO"]);
	// 	array_shift($url);
		
	// 	$quested;
	// 	for ($i = 0; $i < count($url) ; $i++) {
	// 			$quested['domain'] = $this->server_object["SERVER_NAME"];
	// 		if ($i == 0) {
	// 			$quested['controller'] = $url[$i];
	// 		} elseif ($i == 1) {
	// 			$quested['action'] = $url[$i];
	// 		} else {
	// 			$quested['params'][] = $url[$i];
	// 		}
	// 	}
	// 	return $quested;
	// }

	public function filter_url() {
		$url = explode('/', $this->server_object["REQUEST_URI"]);
		array_shift($url);

		$quested;
		for ($i = 0; $i < count($url) ; $i++) {
				$quested['domain'] = $this->server_object["SERVER_NAME"];
			if ($i == 0) {
				$quested['controller'] = $url[$i];
			} elseif ($i == 1) {
				$quested['action'] = $url[$i];
			} else {
				$quested['params'][] = $url[$i];
			}
		}
		return $quested;
	}
}



?>