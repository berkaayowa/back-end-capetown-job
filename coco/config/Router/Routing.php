<?php
namespace coco\config\router;

foreach (glob("Controllers/*.php") as $filename)
{
    require_once($filename);
}

class Routing
{

	function __construct()
	{
		# code...
	}

	public static function to($object) {
		if($object != null) {
			$controller = $object['controller'];
			$controller_calss = "\\controller\\".ucfirst($controller)."Controller";
			if(isset($object['action']) || !empty($object['action'])) {
				$action =$object['action'];
			} else {
				$action =$object['action'] = 'index';
			}

			$controller_path = 'Controllers/'.ucfirst($controller).'Controller.php';
			//echo $controller_path;
			if (file_exists($controller_path)) {
				if(strtolower($controller) == 'generators') {
					$controller_to_call = new $controller_calss();
				} else {
					$controller_to_call = new $controller_calss();
				}

				if (method_exists($controller_to_call,$action)) {
					if(isset($object['params'])) {
						$controller_to_call->$action($object);
					} else {
						$controller_to_call->$action();
					}
				} else {
					
					if (method_exists($controller_to_call,'index')) {
						if (!isset($action) || empty($action)) {
							$controller_to_call->index();
						} else {
							die('Error:: Action -'.$action.'- does not exist in '.$controller_calss);
						}
					} else {
						die('Error:: default Action index missing  in '.$controller_calss);
					}

				}

			} else {
				//die('Error::could not find '.$controller.'Controller');
				//header('Location: /attractions/');
			}
		} else {
			die('Error:: Null Route object passed for Routing');
		}
	}
}
?>