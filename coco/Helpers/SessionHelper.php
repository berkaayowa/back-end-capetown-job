<?php
namespace coco\helpers;

class SessionHelper 
{
	
	function __construct()
	{
		# code...
	}

	public function start() {
		session_start();
	}

	public function add($key, $value) {
		if (!isset($_SESSION)) {
			session_start();
		}
		$_SESSION[$key] = $value;
		return ($_SESSION[$key] == $value) ? true : false;
	}

	public function remove($key) {
		if (!isset($_SESSION)) {
			session_start();
		}

		$value_to_remove = $_SESSION[$key];
		return (array_shift($_SESSION[$key]) == $value_to_remove) ? true : false;	 
	}

	public function get($key) {
		if (!isset($_SESSION)) {
			session_start();
		}
		if (isset($_SESSION[$key])) {
			return $_SESSION[$key];	 
		}
		return null;
		
	}

	public function kill() {
		session_destroy();
	}
}
?>