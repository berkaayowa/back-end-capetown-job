<?php
namespace coco\template;

class AppView
{
	private $data;
	public $variables;
	function __construct($variables='') {
		$this->variables = $variables;
		$this->data = null;
	}

	public function render() {
		$trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
		$view_to_render =  $trace[count($trace) - 1]['function'];
		$called_controller =  $trace[count($trace) - 1]['class'];

		$called_controller = str_replace('Controller','',$called_controller);
		$called_controller = str_replace('controller','',$called_controller);
		$called_controller = str_replace('\\','',$called_controller);
		$called_controller = trim($called_controller);

		$title = $called_controller;
		$template_data = $this->data;

		$topbar = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/Views/Layout/topbar.php');
		$navigation= file_get_contents($_SERVER['DOCUMENT_ROOT'].'/Views/Layout/navigation.php');
		$menu = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/Views/Layout/menu.php');
		$footer = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/Views/Layout/footer.php');

		ob_start();
		require($_SERVER['DOCUMENT_ROOT'].'/Views/'.$called_controller.'/'.$view_to_render.'.php');
		$content = ob_get_contents();
		ob_end_clean();

		$template = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/Views/Layout/layout.php');

		$file = preg_match('/{.*[a-z0-9A-Z]}/', $template, $match) ;

		if ($file) {
		    $match = $match[0];
		    $new_template = str_replace(['{','}'], ['<?php echo $','?>'], $template);
		    eval("?> $new_template <?php ");
		    echo $new_template;
		}

	}

	public function set($name,$data) {
		$this->data[$name] = $data;
	}

	public function get_content($path) {
		$content ='';
		ob_start();
		require($path);
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}


	public function run_render($action) {
		$trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
		$view_to_render =  $action;
		$called_controller =  $trace[count($trace) - 1]['class'];

		$called_controller = str_replace('Controller','',$called_controller);
		$called_controller = str_replace('controller','',$called_controller);
		$called_controller = str_replace('\\','',$called_controller);
		$called_controller = trim($called_controller);

		$title = $called_controller;
		$template_data = $this->data;

		$topbar = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/Views/Layout/topbar.php');
		$navigation= file_get_contents($_SERVER['DOCUMENT_ROOT'].'/Views/Layout/navigation.php');
		$menu = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/Views/Layout/menu.php');
		$footer = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/Views/Layout/footer.php');

		ob_start();
		require($_SERVER['DOCUMENT_ROOT'].'/Views/'.$called_controller.'/'.$view_to_render.'.php');
		$content = ob_get_contents();
		ob_end_clean();

		$template = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/Views/Layout/layout.php');

		$file = preg_match('/{.*[a-z0-9A-Z]}/', $template, $match) ;

		if ($file) {
		    $match = $match[0];
		    $new_template = str_replace(['{','}'], ['<?php echo $','?>'], $template);
		    eval("?> $new_template <?php ");
		    echo $new_template;
		}
	}

}
?>