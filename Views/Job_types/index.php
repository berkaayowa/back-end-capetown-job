<?php $job_types = $template_data['job_types'] ?>
<form method="POST" action="/job_types/search/">
  <div class="input-group">
    <input type="text" class="form-control" placeholder="Search" name="search">
    <div class="input-group-btn">
      <button class="btn btn-default" type="submit">
        <i class="glyphicon glyphicon-search"></i>
      </button>
    </div>
  </div>
</form>
<br>
<div class="table-responsive">
	<table class="table">
		<thead class="thead-inverse">
			<tr>
				<th style='text-transform: capitalize;'>type id</th>
				<th style='text-transform: capitalize;'>type name</th>
				<th>Options</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($job_types as $data  ): ?>
				<tr>
					<td data-limit-char="20"><?=$data["type_id"]?></td>
					<td data-limit-char="20"><?=$data["type_name"]?></td>
					
					<td>
						<a href="/job_types/edit/<?= $data['type_id'] ?>">
							<button type="button" class="btn btn-default">Edite</button>
						</a>
						<a href="/job_types/delete/<?= $data['type_id'] ?>">
							<button type="button" class="btn btn-default">Delete</button>
						</a>
						<a href="/job_types/view/<?= $data['type_id'] ?>">
							<button type="button" class="btn btn-default">View</button>
						</a>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
<script>
	$app.initList(); 
</script>

