<?php $data = $template_data['job'][0]; ?>

<h2 style="margin-top: 0;text-transform: capitalize;">Editing job</h2>
<form method="POST" action="/jobs/edit/<?=$data['job_id']?>">
	<label style='text-transform: capitalize;'>job title</label><br>
	<input type="text" class="form-control" name="job_title" value="<?=$data["job_title"]?>"><br>
	<label style='text-transform: capitalize;'>job decription</label><br>
	<input type="text" class="form-control" name="job_decription" value="<?=$data["job_decription"]?>"><br>
	<label style='text-transform: capitalize;'>job experience</label><br>
	<input type="text" class="form-control" name="job_experience" value="<?=$data["job_experience"]?>"><br>
	<label style='text-transform: capitalize;'>job type id</label><br>
	<input type="text" class="form-control" name="job_type_id" value="<?=$data["job_type_id"]?>"><br>
	<label style='text-transform: capitalize;'>job category id</label><br>
	<input type="text" class="form-control" name="job_category_id" value="<?=$data["job_category_id"]?>"><br>
	<label style='text-transform: capitalize;'>job city id</label><br>
	<input type="text" class="form-control" name="job_city_id" value="<?=$data["job_city_id"]?>"><br>
	<label style='text-transform: capitalize;'>job date posted</label><br>
	<input type="text" class="form-control" name="job_date_posted" value="<?=$data["job_date_posted"]?>"><br>
	
	<input type="hidden" name="job_id" value="<?=$data['job_id']?>">
	<button type="submit" class="btn btn-primary">Save</button>
</form>