<?php $jobs = $template_data['jobs'] ?>
<form method="POST" action="/jobs/search/">
  <div class="input-group">
    <input type="text" class="form-control" placeholder="Search" name="search">
    <div class="input-group-btn">
      <button class="btn btn-default" type="submit">
        <i class="glyphicon glyphicon-search"></i>
      </button>
    </div>
  </div>
</form>
<br>
<div class="table-responsive">
	<table class="table">
		<thead class="thead-inverse">
			<tr>
				<th style='text-transform: capitalize;'>job id</th>
				<th style='text-transform: capitalize;'>job title</th>
				<th style='text-transform: capitalize;'>job decription</th>
				<th style='text-transform: capitalize;'>job experience</th>
				<th style='text-transform: capitalize;'>job type id</th>
				<th style='text-transform: capitalize;'>job category id</th>
				<th style='text-transform: capitalize;'>job city id</th>
				<th style='text-transform: capitalize;'>job date posted</th>
				<th>Options</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($jobs as $data  ): ?>
				<tr>
					<td data-limit-char="20"><?=$data["job_id"]?></td>
					<td data-limit-char="20"><?=$data["job_title"]?></td>
					<td data-limit-char="20"><?=$data["job_decription"]?></td>
					<td data-limit-char="20"><?=$data["job_experience"]?></td>
					<td data-limit-char="20"><?=$data["job_type_id"]?></td>
					<td data-limit-char="20"><?=$data["job_category_id"]?></td>
					<td data-limit-char="20"><?=$data["job_city_id"]?></td>
					<td data-limit-char="20"><?=$data["job_date_posted"]?></td>
					
					<td>
						<a href="/jobs/edit/<?= $data['job_id'] ?>">
							<button type="button" class="btn btn-default">Edite</button>
						</a>
						<a href="/jobs/delete/<?= $data['job_id'] ?>">
							<button type="button" class="btn btn-default">Delete</button>
						</a>
						<a href="/jobs/view/<?= $data['job_id'] ?>">
							<button type="button" class="btn btn-default">View</button>
						</a>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
<script>
	$app.initList(); 
</script>

