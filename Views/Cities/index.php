<?php $cities = $template_data['cities'] ?>
<form method="POST" action="/cities/search/">
  <div class="input-group">
    <input type="text" class="form-control" placeholder="Search" name="search">
    <div class="input-group-btn">
      <button class="btn btn-default" type="submit">
        <i class="glyphicon glyphicon-search"></i>
      </button>
    </div>
  </div>
</form>
<br>
<div class="table-responsive">
	<table class="table">
		<thead class="thead-inverse">
			<tr>
				<th style='text-transform: capitalize;'>city id</th>
				<th style='text-transform: capitalize;'>country id</th>
				<th style='text-transform: capitalize;'>city name</th>
				<th style='text-transform: capitalize;'>ccity decrition</th>
				<th>Options</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($cities as $data  ): ?>
				<tr>
					<td data-limit-char="20"><?=$data["city_id"]?></td>
					<td data-limit-char="20"><?=$data["country_id"]?></td>
					<td data-limit-char="20"><?=$data["city_name"]?></td>
					<td data-limit-char="20"><?=$data["ccity_decrition"]?></td>
					
					<td>
						<a href="/cities/edit/<?= $data['city_id'] ?>">
							<button type="button" class="btn btn-default">Edite</button>
						</a>
						<a href="/cities/delete/<?= $data['city_id'] ?>">
							<button type="button" class="btn btn-default">Delete</button>
						</a>
						<a href="/cities/view/<?= $data['city_id'] ?>">
							<button type="button" class="btn btn-default">View</button>
						</a>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
<script>
	$app.initList(); 
</script>

