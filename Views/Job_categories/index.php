<?php $job_categories = $template_data['job_categories'] ?>
<form method="POST" action="/job_categories/search/">
  <div class="input-group">
    <input type="text" class="form-control" placeholder="Search" name="search">
    <div class="input-group-btn">
      <button class="btn btn-default" type="submit">
        <i class="glyphicon glyphicon-search"></i>
      </button>
    </div>
  </div>
</form>
<br>
<div class="table-responsive">
	<table class="table">
		<thead class="thead-inverse">
			<tr>
				<th style='text-transform: capitalize;'>category id</th>
				<th style='text-transform: capitalize;'>category name</th>
				<th style='text-transform: capitalize;'>category description</th>
				<th>Options</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($job_categories as $data  ): ?>
				<tr>
					<td data-limit-char="20"><?=$data["category_id"]?></td>
					<td data-limit-char="20"><?=$data["category_name"]?></td>
					<td data-limit-char="20"><?=$data["category_description"]?></td>
					
					<td>
						<a href="/job_categories/edit/<?= $data['category_id'] ?>">
							<button type="button" class="btn btn-default">Edite</button>
						</a>
						<a href="/job_categories/delete/<?= $data['category_id'] ?>">
							<button type="button" class="btn btn-default">Delete</button>
						</a>
						<a href="/job_categories/view/<?= $data['category_id'] ?>">
							<button type="button" class="btn btn-default">View</button>
						</a>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
<script>
	$app.initList(); 
</script>

