<?php $data = $template_data['job_category'][0]; ?>

<h2 style="margin-top: 0;text-transform: capitalize;">Editing job_category</h2>
<form method="POST" action="/job_categories/edit/<?=$data['category_id']?>">
	<label style='text-transform: capitalize;'>category name</label><br>
	<input type="text" class="form-control" name="category_name" value="<?=$data["category_name"]?>"><br>
	<label style='text-transform: capitalize;'>category description</label><br>
	<input type="text" class="form-control" name="category_description" value="<?=$data["category_description"]?>"><br>
	
	<input type="hidden" name="category_id" value="<?=$data['category_id']?>">
	<button type="submit" class="btn btn-primary">Save</button>
</form>