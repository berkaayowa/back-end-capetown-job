<?php $countries = $template_data['countries'] ?>
<form method="POST" action="/countries/search/">
  <div class="input-group">
    <input type="text" class="form-control" placeholder="Search" name="search">
    <div class="input-group-btn">
      <button class="btn btn-default" type="submit">
        <i class="glyphicon glyphicon-search"></i>
      </button>
    </div>
  </div>
</form>
<br>
<div class="table-responsive">
	<table class="table">
		<thead class="thead-inverse">
			<tr>
				<th style='text-transform: capitalize;'>country id</th>
				<th style='text-transform: capitalize;'>country name</th>
				<th style='text-transform: capitalize;'>country decription</th>
				<th>Options</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($countries as $data  ): ?>
				<tr>
					<td data-limit-char="20"><?=$data["country_id"]?></td>
					<td data-limit-char="20"><?=$data["country_name"]?></td>
					<td data-limit-char="20"><?=$data["country_decription"]?></td>
					
					<td>
						<a href="/countries/edit/<?= $data['country_id'] ?>">
							<button type="button" class="btn btn-default">Edite</button>
						</a>
						<a href="/countries/delete/<?= $data['country_id'] ?>">
							<button type="button" class="btn btn-default">Delete</button>
						</a>
						<a href="/countries/view/<?= $data['country_id'] ?>">
							<button type="button" class="btn btn-default">View</button>
						</a>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
<script>
	$app.initList(); 
</script>

