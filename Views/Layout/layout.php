<!DOCTYPE html>
<html class="no-js" lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/Views/Layout/css/bootstrap.css">
	<link rel="stylesheet" href="/Views/Layout/css/bootstrap-theme.css">
	<link rel="stylesheet" href="/Views/Layout/css/app.css">
	<title><?= $title ?></title>
	<script type="text/javascript" src="/Views/Layout/js/jquery-3.1.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/Views/Layout/js/app.js"></script>
	<script type="text/javascript">
	</script>
</head>
<body>
	<header class="bs-docs-nav navbar navbar-static-top center" id="top">
		{navigation}
	</header>
	<div class="container">
		<div class="row ">
			<div class="col-sm-12 col-md-12 col-lg-12">
				{content}
			</div>
		</div>
		<div class="row">
			{footer}
		</div>
	</div>
	
	<script src="js/app.js"></script>
</body>
</html>



