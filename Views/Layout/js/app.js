

$app = {
	initAdmin:function(){},
	initHome:function(){},
	initNoAdmin:function(){},
	initList:function(){},
	initImageUpload:function(){},
	initOverlayOptions:function(){}
};

$app.initList = function() {
	$('[data-limit-char]').each(function(){
		var length = $(this).data('limit-char');
		console.log(length);
		$target = $(this).text();
		if($target.length > length) {
			$(this).text($target.substring(0,length)+"...");
		}
	});
}

$app.initImageUpload = function(){
	$upload_btn = $('[data-upload-image]');
	$image = $('#image-box');
	$upload_btn.on('click', function(){
		var upload_link = $('[data-file]').val();
		$image.src=upload_link;
		alert(upload_link);
	});
}



