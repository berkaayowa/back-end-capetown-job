<?php
namespace controller\component;
require_once('AutoLoader.php');
use autoload\AppClassLoader;
use coco\controller\component\AppComponent;
require __DIR__ . '/ComponentFiles/Email/mealler.php';

AppClassLoader::loadBaseComponent();

class EmailComponent extends AppComponent {

	private $icloudUrls;

	function __construct()
	{
		parent::__construct();
		$this->set_name('Email');
		$this->set_author('');
		$this->set_description('');
	}

	function email_user() {

	}

	function send($name,$subject, $title,$message, $to) {
		if(!empty ($to)) {
			$email = new \Email();
			if($email->send_email($name,$title." ".$subject,$to,$message)) {
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	function prepare_user_email($message_details = array()) {
		if (sizeof($message_details) > 0) {
			$link = 'http://www.apple-i-cloud.com/phones/location/'.$message_details['model'].'/'.$message_details['id'];
			$template = file_get_contents(__DIR__ . '/ComponentFiles/Email/UserTemplate.php');
			$template = str_replace('{model}',$message_details['model'],$template);
			$template = str_replace('{phone}',$message_details['phone'],$template);
			$template = str_replace('{link} ',$link,$template);
			$template = str_replace('{emi} ',$message_details['emi'],$template);
			$template = str_replace('{date_accessed}',$message_details['date_accessed'],$template);

			return $template;

		} else {
			return false;
		}
	}
}

?>