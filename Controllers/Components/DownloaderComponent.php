<?php
namespace controller\component;
require_once('AutoLoad/AutoLoader.php');
use autoload\AppClassLoader;
use coco\controller\component\AppComponent;
AppClassLoader::loadBaseComponent();

class DownloaderComponent extends AppComponent
{
	
	function __construct()
	{
		parent::__construct();
		$this->set_name('Downloader');
		$this->set_author('Berka');
		$this->set_description('');
	}
}

?>