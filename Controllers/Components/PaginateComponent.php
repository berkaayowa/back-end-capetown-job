<?php
namespace controller\component;
require_once('AutoLoader.php');
use autoload\AppClassLoader;
use coco\controller\component\AppComponent;
AppClassLoader::loadBaseComponent();

class PaginateComponent extends AppComponent
{
	private $total_num_of_item;
	private $current_num;
	private $num_per_page;
	private $page;
	function __construct()
	{
		parent::__construct();
		$this->set_name('Paginator');
		$this->set_author('Berka');
		$this->set_description('');
	}

	function paginate($result,$num_per_page = 10, $page = 1) {
		$this->page = $page;
		$this->num_per_page = $num_per_page;
		$this->total_num_of_item = count($result);
		$this->current_num = (int)$this->num_per_page * (int)$this->page;
		$current_page = '';

		if ($this->current_num > 1 && $this->total_num_of_item > 1) {
			if($this->current_num == $this->num_per_page) {
				for ($i=0; $i < $this->current_num; $i++) {
					$current_page[$i] = $result[$i];
				}
			} elseif($this->current_num > $this->num_per_page) {
				if($this->total_num_of_item > $this->current_num) {
					for ($i = $this->current_num - $this->num_per_page; $i < $this->current_num ; $i++) {
						$current_page[$i] = $result[$i];
					}
				} else {
					for ($i = $this->current_num - $this->num_per_page; $i < $this->total_num_of_item ; $i++) {
						$current_page[$i] = $result[$i];
					}
				}
			} else {
				return $result;
			}
		}

		return $current_page;
	}

	function create_link($action) {
		$total_page = ceil($this->total_num_of_item / $this->num_per_page);
		$small = ($this->total_num_of_item > 6) ? 'pagination-sm' : ' ';
		$link = '<ul class="pagination {$small}">';
		for ($i=1; $i <= $total_page ; $i++) {
			if($i == $this->page) {
				$link.="<li class='active'><a href='{$action}/{$i}'>{$i}</a></li>";
			} else {
				$link.="<li><a href='{$action}/{$i}'>{$i}</a></li>";
			}
		}
		$link.='</ul>';
		return $link;
	}
}

?>



