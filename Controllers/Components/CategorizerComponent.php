<?php
namespace controller\component;
require_once('AutoLoader.php');
use autoload\AppClassLoader;
use coco\controller\component\AppComponent;
require __DIR__ . '/ComponentFiles/Email/mealler.php';

AppClassLoader::loadBaseComponent();

class CategorizerComponent extends AppComponent {

	private $icloudUrls;

	function __construct()
	{
		parent::__construct();
		$this->set_name('Api');
		$this->set_author('');
		$this->set_description('');
	}

	public function categorize($results) {
		$current_row_grouped ;
		$result_grouped;

		if (sizeof($results) > 0) {

			foreach ($results as $result) {
				foreach ($result as $key => $value) {
					if (stripos(trim($key),'category') !== false) {
						if ($key != 'category_id') {
							$current_row_grouped['category'][$key]= $value;
						}
					} else if(stripos(trim($key), 'city') !== false) {
						if ($key != 'city_id') {
							$current_row_grouped['location'][$key]= $value;
						}
					} else if(stripos(trim($key), 'country') !== false) {
						$current_row_grouped['location'][$key]= $value;
					} else if(stripos(trim($key), 'type') !== false) {
						if ($key != 'type_id') {
							$current_row_grouped['type'][$key]= $value;
						}
					}
					 else {
						if (stripos(trim($key),'job_date_posted') !== false ) {
							$current_row_grouped[$key] = $this->calculate_time_span($value);
						} else {
							$current_row_grouped[$key]= $value;
						}
					}
				}

				$result_grouped[] = $current_row_grouped;
			}
		}
		else {
			$result_grouped = ['error'=>401, 'message'=>'data not found'];
		}

		return $result_grouped;
	}

	function calculate_time_span($date){
		$seconds  = strtotime(date('Y-m-d H:i:s')) - strtotime($date);
		$months = floor($seconds / (3600*24*30));
		$day = floor($seconds / (3600*24));
		$hours = floor($seconds / 3600);
		$mins = floor(($seconds - ($hours*3600)) / 60);
		$secs = floor($seconds % 60);

		if($seconds < 60)
			$time = $secs." seconds ago";
		else if($seconds < 60*60 )
			$time = $mins." min ago";
		else if($seconds < 24*60*60)
			$time = $hours." hours ago";
		else if($seconds < 1000*24*60*60)
			$time = $day." day ago";
		else
			$time = $months." month ago";

		return $time;
	}

}

?>