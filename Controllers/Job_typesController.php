<?php
	namespace controller;
	require_once('AutoLoader.php');
	use autoload\AppClassLoader;
	AppClassLoader::loadControllerRequires();
	use coco\Controller\AppController;
	use coco\template\AppView;

	class Job_typesController extends AppController
	{
		private $flash;
		private $paginate;

		function __construct() {
			parent::__construct();
			$this->flash = $this->load_component('Flash');
			$this->paginate = $this->load_component('Paginate');
		}

		function index() {
			$result = $this->model->fetch_all();
			$this->appView->set('job_types', $result);
			$this->appView->render();
		}

		function add() {
			if($this->is_set($this->get_post())) {
				if ($this->model->add($this->get_post())) {
					$this->flash->success(' Saved job_type');
				} else {
					$this->flash->error(' Could not Saved job_type !');
				}
			}

			$this->appView->render();
		}

		function edit($params) {
			$id = $params['params'][0];

			if($this->is_set($this->get_post())) {
				if ($this->model->update($this->get_post())) {
					$this->flash->success(' Edited job_type');
				} else {
					$this->flash->error(' Could not Edit job_type !');
				}
			}

			$result = $this->model->fetch_by(['type_id'=>$id]);
			$this->appView->set('job_type',$result);
			$this->appView->render();
		}

		function delete($params) {
			$id = $params['params'][0];
			if($this->model->delete($id)) {
				$this->flash->success(' Deleted job_type');
			} else {
				$this->flash->error(' Could not Delete job_type !');
			}

			$this->index();
		}

		function view($params) {
			$id = $params['params'][0];

			$result = $this->model->fetch_by(['type_id'=>$id]);
			$this->appView->set('job_type',$result);
			$this->appView->render();
		}

		function api() {
			return $this->json_format($this->model->fetch_all());
		}

		function search() {
			$tag = $this->get_POST_key('search');
			$result = $this->model->fetch_like($tag);
			$this->appView->set('job_types',$result);
			$this->appView->run_render('index');
		}

	}

?>