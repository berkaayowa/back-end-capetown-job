<?php
	namespace controller;
	require_once('AutoLoader.php');
	use autoload\AppClassLoader;
	AppClassLoader::loadControllerRequires();
	use coco\Controller\AppController;
	use coco\template\AppView;

	class EmailsController extends AppController
	{
		private $flash;
		private $paginate;

		function __construct() {
			parent::__construct();
			$this->flash = $this->load_component('Flash');
			$this->paginate = $this->load_component('Paginate');
		}

		function index($params = '') {
			$result = $this->model->fetch_all();
			$page = !empty($params) ? $params[0] : 1;
			$result_paginated = $this->paginate->paginate($result,6,$page);
			$this->appView->set('emails', $result_paginated);
			$this->appView->set('paginate_links', $this->paginate->create_link('/emails/index'));
			$this->appView->render();
		}

		function add() {
			if($this->is_set($this->get_post())) {
				if ($this->model->add($this->get_post())) {
					$this->flash->success(' Saved email');
				} else {
					$this->flash->error(' Could not Saved email !');
				}
			}

			$this->appView->render();
		}

		function edit($params) {
			$id = $params[0];

			if($this->is_set($this->get_post())) {
				if ($this->model->update($this->get_post())) {
					$this->flash->success(' Edited email');
				} else {
					$this->flash->error(' Could not Edit email !');
				}
			}

			$result = $this->model->fetch_by(['id'=>$id]);
			$this->appView->set('email',$result);
			$this->appView->render();
		}

		function delete($params) {
			$id = $params[0];
			if($this->model->delete($id)) {
				$this->flash->success(' Deleted email');
			} else {
				$this->flash->error(' Could not Delete email !');
			}

			$this->index();
		}

		function view($params) {
			$id = $params[0];

			$result = $this->model->fetch_by(['id'=>$id]);
			$this->appView->set('email',$result);
			$this->appView->render();
		}

		function api() {
			return $this->json_format($this->model->fetch_all());
		}

		function search() {
			$tag = $this->get_POST_key('search');
			$result = $this->model->fetch_like($tag);
			$this->appView->set('emails',$result);
			$this->appView->run_render('index');
		}

	}

?>