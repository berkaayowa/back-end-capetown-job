<?php
	namespace controller;
	require_once('AutoLoader.php');
	use autoload\AppClassLoader;
	AppClassLoader::loadControllerRequires();
	use coco\Controller\AppController;
	use coco\template\AppView;

	class JobsController extends AppController
	{
		private $flash;
		private $paginate;
		private $categorizer;

		function __construct() {
			parent::__construct();
			$this->flash = $this->load_component('Flash');
			$this->paginate = $this->load_component('Paginate');
			$this->categorizer = $this->load_component('Categorizer');
		}

		function index() {
			$results = $this->model->fetch_all();
			return $this->json_format(['jobs'=>$this->categorizer->categorize($results)]);
		}

		function add() {
			if($this->is_set($this->get_post())) {
				if ($this->model->add($this->get_post())) {
					$this->flash->success(' Saved job');
				} else {
					$this->flash->error(' Could not Saved job !');
				}
			}

			$this->appView->render();
		}

		function edit($params) {
			$id = $params['params'][0];

			if($this->is_set($this->get_post())) {
				if ($this->model->update($this->get_post())) {
					$this->flash->success(' Edited job');
				} else {
					$this->flash->error(' Could not Edit job !');
				}
			}

			$result = $this->model->fetch_by(['job_id'=>$id]);
			$this->appView->set('job',$result);
			$this->appView->render();
		}

		function delete($params) {
			$id = $params['params'][0];
			if($this->model->delete($id)) {
				$this->flash->success(' Deleted job');
			} else {
				$this->flash->error(' Could not Delete job !');
			}

			$this->index();
		}

		function view($params) {
			$id = $params['params'][0];

			$result = $this->model->fetch_by(['job_id'=>$id]);
			$this->appView->set('job',$result);
			$this->appView->render();
		}

		function api() {
			return $this->json_format($this->model->fetch_all());
		}

		function search($params) {
			$tag = $params['params'][0];
			//$options = $params['options']['order'];
			//print_r($params);
			$result = $this->model->fetch_like(['job_title'=>$tag]);
			return $this->json_format(['jobs'=>$this->categorizer->categorize($result)]);
		}

	}

?>